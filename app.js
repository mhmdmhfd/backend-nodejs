const express = require('express')
const bodyParser = require('body-parser')
const session = require('express-session')
const bcrypt = require("bcrypt-nodejs")
const fs = require('fs')
const TOTP = require('./TOTP.js')

const myTOTP = new TOTP('SHAREDSECRETPLUSULTRA')
myTOTP.qrcode('PlusUltra', 'PlusUltraInc')

const app = express()
app.set('view engine', 'ejs')

app.use(bodyParser.json())

app.use(session({
    secret: 'whocaresaboutastrongpassword?',
    resave: false,
    saveUninitialized: false,
    cookie: { maxAge : 60000 }
}))

global.calendar = null
global.users = null

app.listen(8080, () => {
    console.log('Application démarrée sur le port 8080 !')
    fs.readFile('calendar.json', (err, data) => {
        if (err) throw err
        calendar = data.toString('utf8')
    })

    fs.readFile('users.json', (err, data) => {
        if (err) throw err
        users = data.toString('utf8')
    })
})

app.get('/', (req, res) => {
    console.log('/')
    res.send(JSON.parse(calendar))
})

app.get('/event', (req, res) => {
    console.log('/event')

    var item = JSON.parse(calendar).map((currentValue) => {
        return JSON.parse(currentValue.id)
    })

    res.json(item)
})

app.get('/event/:id', (req, res) => {
    console.log("/event/" + req.params.id)

    var item = null

    JSON.parse(calendar).forEach((element) => {
        if (JSON.parse(element.id) == req.params.id) {
            item = element
        }
    })
    if (item) {
        res.json(item)
    } else {
        res.sendStatus(404)
    }
})

app.get('/users', (req, res) => {
    res.json(JSON.parse(users))
})

app.post('/user/login', (req, res) => {
    console.log("POST /user/login")

    username = req.body.id
    password = req.body.passwd
    number = req.body.number

    console.log(username, password, number)

    JSON.parse(users).forEach((user) => {
        if (user.username == req.body.id) {
            console.log("user = "+user.username)
            bcrypt.compare(req.body.passwd, user.password, (err, isok) => {
                if (isok) {
                    console.log(user.username+" is OK")

                    console.log(myTOTP.now())

                    myTOTP.verify(number)
                        .then(valid => console.log('valid ?', valid))

                    res.json("{ login : accepted}")


                } else {
                    console.log("KO")
                    res.json('{ login : denied }')
                }
            })
        }
    })
})

app.get('/cook', function(err, res) {
    res.redirect('/cookie')
})

app.get('/cookie', function(req, res, next) {
    console.log("GET /cookie")

    if (req.session.views) {
        req.session.views++
        res.setHeader('Content-Type', 'text/html')
        res.write('<p>views: ' + req.session.views + '</p>')
        res.write('<p>expires in: ' + (req.session.cookie.maxAge / 1000) + 's</p>')
        res.end()
    } else {
        req.session.views = 1
        res.end('welcome to the session demo. refresh!')
    }

    console.log(req.session)
})

app.get('/user/logout', function (req, res, next) {
    console.log("GET /user/logout")

    req.session.destroy(function(err) {
        if (err) throw err
    })
    res.send("OK")

    console.log(req.session)
})

app.get('/qrcode', function(req, res, next) {
    console.log("GET /qrcode")


})